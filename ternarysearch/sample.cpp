#include <vector>
#include <algorithm>
#include <sstream>
#include <string>
#include <random>
#include <chrono>
#include <iostream>
#include <ctime>
#include <cstdlib>

// Function to perform Ternary Search 
long long ternarySearchaux(const std::vector<long long > &data, int value, int l, int r) 
{ 
    if (r >= l) {   
        // Find mid1 and mid2 
        unsigned int mid1 = l + (r - l) / 3; 
        unsigned int mid2 = r - (r - l) / 3; 
        // Check if key is present at any mid 
        if (data[mid1] == value) { 
            return mid1; 
        } 
        if (data[mid2] == value) { 
            return mid2; 
        } 
        //run ternary
        if (value < data[mid1]) { 
            // value between l and mid1 
            return ternarySearchaux(data, value, l, mid1 - 1); 
        } 
        else if (value > data[mid2]) { 
            // value between mid2 and r 
            return ternarySearchaux(data, value, mid2 + 1, r); 
        } 
        else { 
            // value between mid1 and mid2 
            return ternarySearchaux(data, value, mid1 + 1, mid2 - 1); 
        } 
    }
    return -1; 
}
long long ternary_search( const std::vector< long long > &data, int value )
{
  return ternarySearchaux(data, value, 0, data.size());
}


long long binary_search_aux( const std::vector< long long> &nums, int value, unsigned int l, unsigned int r ) {
  unsigned int i = (l + r) / 2;
  if (nums[i] < value)
  {
    return binary_search_aux(nums, value, i+1, r);
  }
  else if (nums[i] > value)
  {
    return binary_search_aux(nums, value, l, i);
  }
  else
  {
    return i;
  }
  
}
long long binary_search( const std::vector<long long> &data, int value )
{
  return binary_search_aux(data, value, 0, data.size());
}

long long binary_searchloop( const std::vector<long long > &nums, int value ) {
  unsigned int low = 1;
  unsigned int high = nums.size(); 
  bool found = false;
  long long returnval;
 
  while (!found)
  {
    unsigned int midPoint = low + ( high - low ) / 2;
    if (nums[midPoint] < value) {
      low = midPoint + 1;
    }
    if (nums[midPoint] > value) {
      high = midPoint - 1; 
    }
    if (nums[midPoint] == value) {
      found = true;
      returnval = midPoint;
    } 
  }
  return returnval;
}
std::vector<long long> createVector( unsigned int exponent) {
	std::vector<long long> nums;
	long long n = pow(2,exponent); 
  for (long long i = 1; i <= n; ++i) {
    nums.push_back(i);
  }
  return nums;
}

int main(int argc, char **argv)
{
  for (unsigned int i = 0; i <= 30; i++)
  {
    /*
    std::vector<long long> nums = createVector(i);

    int reps = 2^30;
    unsigned int value = i + 1;
    clock_t t1 = clock();
    binary_search(nums,value);
    clock_t t2 = clock();
    clock_t dt = t2 - t1;
    double clocks_per_rep = ((double)dt)/reps;
    double seconds = clocks_per_rep/CLOCKS_PER_SEC;

    std::cout << seconds << std::endl;*/
    std::vector<long long> nums = createVector(i);

    int reps = 2^30;
    unsigned int value = i + 1;
    clock_t t3 = clock();
    ternary_search(nums,value);
    clock_t t4 = clock();
    clock_t difft = t4 - t3;
    double clocks_per_rep = ((double)difft)/reps;
    double seconds2 = clocks_per_rep/CLOCKS_PER_SEC;

    std::cout  << seconds2 << std::endl;
  }

  return 0;

}
