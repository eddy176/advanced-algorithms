#!/bin/bash

reps=1000000
for i in 1 2 4 8 16 32; do
    ./sample $i $reps
done

reps=1000
for i in 64 128 256 512 1024 2048; do
    ./sample $i $reps
done

reps=10
for i in 4096 8192; do
    ./sample $i $reps
done

reps=1
for i in 10000 20000 40000 80000 1000000 2000000 4000000; do
    ./sample $i $reps
done
