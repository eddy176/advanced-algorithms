#include "bheap.h"
#include <iostream>
#include <cstdlib>
#include <vector>
#include <iterator>
#include <algorithm>

void BHeap::Insert(int x) 
{
   int i = heap.size() - 1; // insert into next open spot in vector
   std::cout<<"about to bubble up"<<std::endl;
   bubbleup(x,i); //bubble up while key less than parents
   //in general make sure you are not going passed the end when checking parents and children
}

void BHeap::decreasekey(int x) 
{
   int i = 0;
   while (heap[i] != x)
      i++;
   bubbleup(x,i);
}

void BHeap::DeleteMin() {
   if (heap.size() == 0)
      return;
   int temp = heap[0];
   heap[0] = heap[heap.size() - 1];
   heap[heap.size()- 1] = temp;
   heap.pop_back();
   showHeap();
   siftdown(heap[heap.size() - 1], 1);
   std::cout<<"Element Deleted and Heap sifted"<<std::endl;
   showHeap();
}

void BHeap::bubbleup(int x, int i) {
   int p = i-1/2;
   while ( i != 1 && (heap[p] > heap[i]))
   {
      std::cout<<"bubbling"<<std::endl;
      heap[i] = heap[p];
      i = p;
      p = i/2;
      std::cout<<"i:" << i <<std::endl;
      std::cout<<"p:" << p <<std::endl;

   }
   heap[i] = x;
}

void BHeap::siftdown(int x, int i) {
   
   int child = minchild(i);
   std::cout<<"child: " << child <<std::endl;
   while ( child != 0 && (heap[child] < heap[i]))
   {
      heap[i] = heap[child];
      i = child;
      child = minchild(i);
   }
   heap[i] = x;

}

int BHeap::minchild(int i) {
   int left = 2*i + 1;
   int right = 2*i + 2;
   int low = 10000;
   if (2*i > heap.size() - 1) {
      return -1;
   }
   else
   {
      for ( int i = 0; i < heap.size(); i++)
      {
         if (low > heap[left])
            low = left;
         if (low > heap[right])
            low = right;
      }
      return low;
   }
}

int BHeap::left(int parent) {
   int l = 2 * parent + 1;
   if (l < heap.size())
      return l;
   else
      return -1;
}
int BHeap::right(int parent) {
   int r = 2 * parent + 2;
   if (r < heap.size())
      return r;
   else
      return -1;
}


void BHeap::makeHeap(int s) {
   
   for (int x = 0; x < s; x++)
   {
      heap.push_back(x);
   }
   for (int i = heap.size() - 1; i <= 1; i--)
   {
      siftdown(heap[i], i);
   }
   /*while (pos != heap.end()) {
        siftdown(heap[pos] = pos);
        std::cout<<*pos<<" ";
        pos++;
   }*/
}

void BHeap::showHeap() {
   std::cout<<"Heap  "<<std::endl;
   for (int x = 0; x < heap.size(); x++)
   {
      std::cout<<heap[x]<<std::endl;
   }
}

int main() {
   BHeap h;
   while (1) {
      int c, e, x;
      std::cout<<"1.Insert Element"<<std::endl;
      std::cout<<"2.Delete Minimum Element"<<std::endl;
      std::cout<<"3.Extract Minimum Element"<<std::endl;
      std::cout<<"4.Make Heap"<<std::endl;
      std::cout<<"5.Show Heap"<<std::endl;
      std::cout<<"6.Exit"<<std::endl;
      std::cout<<"Enter your choice: ";
      std::cin>>c;
      switch(c) {
         case 1:
            std::cout<<"Enter the element to be inserted: ";
            std::cin>>e;
            h.Insert(e);
         break;
         case 2:
            h.DeleteMin();
         break;
         case 3:
            std::cout<<"Which parent node? : ";
            std::cin>>e;
            if (h.minchild(e) == -1) {
               std::cout<<"Heap is Empty"<<std::endl;
            }
            else
            std::cout<<"Minimum Element: "<<h.minchild(e)<<std::endl;
         break;
         case 4:
            std::cout<<"Enter size: ";
            std::cin>>e;
            h.makeHeap(e);
         break;
         case 5:
            h.showHeap();
            break;
         case 6:
            exit(1);
         default:
            std::cout<<"Enter Correct Choice"<<std::endl;
            break;
      }
   }
   return 0;
}