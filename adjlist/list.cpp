#include "list.h"
#include<vector> 
#include<iostream>

Graph::Graph(std::vector<Edge> const &edges, int V) {
    // resize vector to V
    adjList.resize(V);

    // add edges
    for (auto &edge: edges)
    {
        adjList[edge.src].push_back(edge.dest);
        adjList[edge.dest].push_back(edge.src);
    }
}

// prints representation of graph
void Graph::printGraph(Graph const &graph, int V) 
{ 
    for (int j = 1; j < V; j++) 
    { 
        std::cout << "Adjacency list for vertex " << j <<std::endl; 
        for (auto x : adjList[j])
        {
            std::cout  << x <<std::endl; 
        }
    } 
}

// Function to perform DFS Traversal
void Graph::DFS(Graph const &graph, std::vector<bool> &visited, std::vector<int> &pre, std::vector<int> &post)
{
    int start = 1;

    //mark all vertices not visited
    for (int i = 0; i <= visited.size(); i++)
    {
        visited[i] = false;
    }

    //if vertex not visited explore    
    for (int i = 0; i < visited.size(); i++)
    {
        if(!visited[i])
        {
            explore(graph, i, start, visited, pre, post);
        }
        
    }
}

void Graph::explore(Graph const &graph, int &v, int &i, std::vector<bool> &visited, std::vector<int> &pre, std::vector<int> &post)
{

    //current node visited
    visited[v] = true;
    //previsit
    pre.push_back(i);
    i++;
    // print node
    if (v >= 1)
        std::cout << v << " "<<std::endl;

    // for every edge (v > u)
    for (int u : graph.adjList[v])
    {
        // u is not discovered
        if (!visited[u])
            explore(graph, u, i, visited, pre, post);
    //postvisit
    post.push_back(i);


    }
    i ++;
        
}

int main() 
{ 
    int V = 13; 
    std::vector<bool> visited(V);
    std::vector<int> pre;
    std::vector<int> post;
    
    //figure 3.2
	std::vector<Edge> edges = {
		{1, 2}, {1, 3}, {2, 5}, {2, 6}, {3, 6}, {1, 4},
		{4, 7}, {4, 8}, {5, 9}, {5, 10}, {7, 8}, {9, 10}, {11, 12}
	};
    Graph g(edges, V);
    //g.printGraph(g, V);

    g.DFS(g, visited, pre, post);
    for (int i = 0; i < pre.size() ; i++) {
        std::cout<< "pre: " <<pre[i]<<std::endl;
    }
    for (int i = 0; i < post.size(); i++) {
        std::cout<< "post: " <<post[i]<<std::endl;
    }
    return 0; 
} 