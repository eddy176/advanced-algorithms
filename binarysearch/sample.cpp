#include <vector>
#include <algorithm>
#include <sstream>
#include <string>
#include <random>
#include <chrono>
#include <iostream>
#include <ctime>
#include <cstdlib>
/*psuedo
a = sorted array
x = desired value
int binarySearch(a, x) 
    b = beginning of a
    e = end of a
    if end >= beginning
        mid = nums.size() / 2; 
  
        if a[mid] == x
            mid at location x
  
        if a[mid] > x
            mid -= 1
            return binarySearch(a, x); 

        mid += 1
        return binarySearch(a, x); 
    
    return -1; 
correct because it checks to make sure end of sorted array is larger than beginning ensuring size of array is > 0
also checks index at middle of array and returns if it equals desired value which is correct
changes ranges of array based on value at the middle index of array compared to desired value
*/
/* Big-oh estimate
T(n) = T(n/2) + c
T(n/2) = T(n/4) + c (plug into T(n))
T(n) = T(n/4) + 2c
T(n/4) =  T(n/8) + c (plug into T(n))
T(n) = T(n/8) + 3c
T(n) = T(n / (2^k)) + kc
n = 2^k
T(n / (2^k)) = T(1) & kc = log2(n)c
T(n) = T(1) + log2(n)c
T(n) = c + log2(n)c
T(n) = c(1 + log2(n))
T(n) = log2(n)
*/
unsigned int binary_search_recur( const std::vector< int > &nums, int value ) {
  unsigned int b = nums.front();
  unsigned int e = nums.back();
  if (e >= b)
  {
    unsigned int mid = nums.size()/ 2; 

    if (nums[mid] == value)
    {
      return mid;
    }
    if (nums[mid] > value)
    {
        std::vector< int > array1;
        for (unsigned int i = 0; i < mid; i++)
        {
          array1.push_back(nums[i]);
        }

        return binary_search_recur(array1, value); 
    }

    std::vector< int > array2;
    for (unsigned int i = mid + 1; i < nums.size(); i++)
    {
      array2.push_back(nums[i]);
    }
    return binary_search_recur(array2, value); 
  }
  return 0;

}
unsigned int binary_search( const std::vector< int > &nums, int value ) {
  unsigned int low = 1;
  unsigned int high = nums.size(); 
  bool found = false;
  unsigned int returnval;

  while (!found)
  {
    unsigned int midPoint = low + ( high - low ) / 2;
    if (nums[midPoint] < value) {
      low = midPoint + 1;
    }
    if (nums[midPoint] > value) {
      high = midPoint - 1; 
    }
    if (nums[midPoint] == value) {
      found = true;
      returnval = midPoint;
    } 
  }
  return returnval;
}

int main(int argc, char **argv)
{
  std::vector<int> nums;
  for (int i = 0; i < 100000; i++) {
    nums.push_back(i);
  }
  int reps = 40;
  int value = 97377;
  clock_t t1 = clock();
  std::cout<< binary_search_recur(nums,value) <<std::endl;
  clock_t t2 = clock();
  clock_t dt = t2 - t1;
  double clocks_per_rep = ((double)dt)/reps;
  double seconds = clocks_per_rep/CLOCKS_PER_SEC;

  std::cout << "dt: " << dt << " " << "seconds " << seconds << std::endl;
  return 0;

}
