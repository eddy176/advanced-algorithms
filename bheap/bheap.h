#ifndef BHEAP_H
#define BHEAP_H

#include <vector>

class BHeap {
   private:
      
      int left(int parent);
      int right(int parent);
      void bubbleup(int x, int i);
      void siftdown(int x, int i);
   public:
      BHeap() {}
      std::vector<int> heap;
      void Insert(int x);
      void decreasekey(int x);
      void DeleteMin();
      int minchild(int i);
      void makeHeap(int s);
      void showHeap();
};




#endif