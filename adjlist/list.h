#ifndef LIST_H
#define LIST_H

#include<vector> 

//store graph edges
struct Edge {
	int src;
    int dest;
};

class Graph {

    public:
        std::vector<std::vector<int>> adjList;
        Graph(std::vector<Edge> const &edges, int V);
        void DFS(Graph const &graph, std::vector<bool> &visited, std::vector<int> &pre, std::vector<int> &post);
        void explore(Graph const &graph, int &v, int &i, std::vector<bool> &visited, std::vector<int> &pre, std::vector<int> &post);
        void printGraph(Graph const &graph, int V);

};

#endif